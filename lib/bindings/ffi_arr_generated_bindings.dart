// ignore_for_file: always_specify_types
// ignore_for_file: camel_case_types
// ignore_for_file: non_constant_identifier_names

// AUTO GENERATED FILE, DO NOT EDIT.
//
// Generated by `package:ffigen`.
import 'dart:ffi' as ffi;

/// Bindings for `ffi_arr.h`.
///
/// Regenerate bindings with `flutter pub run ffigen --config ffi_arr.yaml`.
///
class FfiArrBindings {
  /// Holds the symbol lookup function.
  final ffi.Pointer<T> Function<T extends ffi.NativeType>(String symbolName)
      _lookup;

  /// The symbols are looked up in [dynamicLibrary].
  FfiArrBindings(ffi.DynamicLibrary dynamicLibrary)
      : _lookup = dynamicLibrary.lookup;

  /// The symbols are looked up with [lookup].
  FfiArrBindings.fromLookup(
      ffi.Pointer<T> Function<T extends ffi.NativeType>(String symbolName)
          lookup)
      : _lookup = lookup;

  /// linux-specific: END
  int print_arr(
    ffi.Pointer<ffi.Int16> lst,
    int off,
    int len,
  ) {
    return _print_arr(
      lst,
      off,
      len,
    );
  }

  late final _print_arrPtr = _lookup<
      ffi.NativeFunction<
          ffi.IntPtr Function(
              ffi.Pointer<ffi.Int16>, ffi.Int, ffi.Int)>>('print_arr');
  late final _print_arr = _print_arrPtr
      .asFunction<int Function(ffi.Pointer<ffi.Int16>, int, int)>();
}
