#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// linux-specific: START
#include <pthread.h>
#include <unistd.h>

#define FFI_PLUGIN_EXPORT
// linux-specific: END



FFI_PLUGIN_EXPORT intptr_t print_arr(int16_t * lst, int off, int len);
